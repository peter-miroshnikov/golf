defmodule KfGolf do
  def calculate(str) do
    {:ok, tokens, _} = :calc_lexer.string(str)
    IO.inspect(tokens)
    :calc_parser.parse(tokens)
  end
end
