Nonterminals line elem number.
Terminals int atom '+' '-' '*' ')' '(' '/'.
Rootsymbol line.

line -> line '+' elem : {'+', '$1', '$3'}.
line -> line '-' elem : {'-', '$1', '$3'}.
line -> line '/' line : {'/', '$1', '$3'}.
line -> elem          : '$1'.

elem -> '-' elem         : {'*' ,'$2', -1}.
elem -> '(' line ')'     : {'brackets', '$2'}.
elem -> number '*' elem  : {'*', '$1', '$3'}.
elem -> number           : '$1'.

number -> int     : extract_token('$1').


Erlang code.

extract_token({_Token, _Line, Value}) -> Value.
