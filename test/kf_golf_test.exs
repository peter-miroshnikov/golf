defmodule KfGolfTest do
  use ExUnit.Case

test "1+2" do
    assert KfGolf.calculate('1 + 2') == {:ok, {:+, 1, 2}}
  end
  test "1-2" do
    assert KfGolf.calculate('1 - 2') == {:ok, {:-, 1, 2}}
  end

  test "1+2+3" do
    assert KfGolf.calculate('1 + 2 + 3') == {:ok, {:+, {:+, 1, 2}, 3}}
  end

  test "1+2+3 brackets" do
    assert KfGolf.calculate('(1 + 2) + 3') == {:ok, {:+, {:brackets, {:+, 1, 2}}, 3}}
  end

  test "-1+2" do
    assert KfGolf.calculate('-1 + 2') == {:ok, {:+, {:*, 1, -1}, 2}}
  end

  test "-1+2-3" do
    assert KfGolf.calculate('-1 + 2 - 3') == {:ok, {:-, {:+, {:*, 1, -1}, 2}, 3}}
  end

  test "-1+2-3 brackets" do
    assert KfGolf.calculate('-1 + (2 - 3)') == {:ok, {:+, {:*, 1, -1}, {:brackets, {:-, 2, 3}}}}
  end

  test "-1+2-3 brackets2" do
    assert KfGolf.calculate('-1 + (2 - 3) - 1 + (5 + 4 - 2)') ==  {:ok, {:+, {:-, {:+, {:*, 1, -1}, {:brackets, {:-, 2, 3}}}, 1}, {:brackets, {:-, {:+, 5, 4}, 2}}}}
  end

  test "multiplication" do
    assert KfGolf.calculate('2*3') == {:ok, {:*, 2, 3}}
  end

  test "milti 2" do
    assert KfGolf.calculate('1 + 2 * 3 - 3') == {:ok, {:-, {:+, 1, {:*, 2, 3}}, 3}}
  end

  test "milti 3" do
    assert KfGolf.calculate('1 + 2 * 3 - 3 * 7') == {:ok, {:-, {:+, 1, {:*, 2, 3}}, {:*, 3, 7}}}
  end

  test "div" do
    assert KfGolf.calculate('1 / 2') == {:ok, {:/, 1, 2}}
  end

  test "div2" do
    assert KfGolf.calculate('1 + 3 / 2') == {:ok, {:/, {:+, 1, 3}, 2}}
  end

  test "ultimate  0.1" do
    assert KfGolf.calculate('2*(4+ 6) +26') ==  {:ok, {:+, {:*, 2, {:brackets, {:+, 4, 6}}}, 26}}
  end

  test "ultimate  0.2" do
    assert KfGolf.calculate('20 / 2*(4+ 6) +26') == {:ok, {:/, 20, {:+, {:*, 2, {:brackets, {:+, 4, 6}}}, 26}}}
  end

  test "ultimate  0.3" do
    assert KfGolf.calculate('(20 /2*(4+ 6) +26)/ 3*-1') == {:ok, {:/, {:brackets, {:/, 20, {:+, {:*, 2, {:brackets, {:+, 4, 6}}}, 26}}}, {:*, 3, {:*, 1, -1}}}}
  end

  test "ultimate  0.4" do
    assert KfGolf.calculate('-(20 /2*(4+ 6) +26)/ 3*-1') ==  {:ok, {:/, {:*, {:brackets, {:/, 20, {:+, {:*, 2, {:brackets, {:+, 4, 6}}}, 26}}}, -1}, {:*, 3, {:*, 1, -1}}}}
  end
end
